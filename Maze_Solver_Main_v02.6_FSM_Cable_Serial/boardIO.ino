using namespace boardIOKeywords;

void initializeSerial(){
  Serial.begin(_BAUDRATE);
  int i = 0;

  //Testing - say hello
  //debugPrint("hello???");
  //Wait for the other guy to say hello to you!
  while (Serial.available() <= 0);
  String resp = readSerialLine();
  

  if (resp.equals(acknowledge) == false){
    //that's wack
    }
  //Ask if you want to set up in test mode, or in normal mode
  
  
  Serial.println(testCommand);
  while (Serial.available() <= 0);
  //Response will be yes, no, or something wack
  resp = readSerialLine();
  
  
  if (resp.equals(yes)){
    //Acknowledge, and start setting each array cell as their values
    Serial.println(acknowledge + dl + sendMap);
    testingMode = true;
    populateMazeArray();
    
    } else {
    Serial.println(acknowledge);
    testingMode = false;  
    }
  
  }

void debugPrint(String stringIn){
  String stringOut = "";
  stringOut += comment + dl;
  stringOut += stringIn;
  Serial.println(stringOut);
  }

void debugPrint(char * stringIn){
  debugPrint(String(stringIn));
  
  }


void echoPrint(String stringIn){
  String stringOut = "";
  stringOut += biok::echo + biok::dl +stringIn;
  debugPrint(stringOut);
  }

  void echoPrint(char * stringIn){
    echoPrint(String(stringIn));
    }

String readSerialLine(){
  String out = Serial.readStringUntil('\n');
  out.trim();
  if (echoing == true) echoPrint(out);
  return out;
  }


void populateMazeArray(){

  
    String resp = "";
    
    do {
      //Tell it ur ready
      Serial.println(acknowledge);
      while (Serial.available() < 1);
      
      resp = readSerialLine();

      processLine(resp);
        
      }while (resp.equals(nAcknowledge) != true);
      if (ioDebug == true) debugPrint("Done updating!");
  
  }



void updatePosition(int xIn, int yIn, uint8_t cellVal){
  Coordinate posIn(xIn, yIn);
  myMaze.updatePosAt(posIn, cellVal);

  if (ioDebug == true) {
     Serial.print(comment + dl);
     Serial.print("x ");
     Serial.print(xIn);
     Serial.print(" y ");
     Serial.print(yIn);
     Serial.print(" cell val ");
     Serial.println(cellVal, HEX);
    }
  }

void sendPosition(Coordinate pos){

  uint8_t sendCell = myMaze.cellVals(pos);
  sendPositionCellVal(pos, sendCell);
  
  
  }

 void sendPositionCellVal(Coordinate pos, uint8_t cellVal){
  String toSend = "";
  int xSend, ySend;
  

  xSend = pos.getX();
  ySend = pos.getY();
  
  toSend += positionUpdate + dl + xSend + dl + ySend + dl;
  Serial.print(toSend);
  Serial.println(cellVal, DEC);
  
  }

  void sendPositionNotSetCoords(Coordinate pos){
    uint8_t sendCell = myMaze.cellVals(pos);
    sendPositionCellValNotSetCoords(pos, sendCell);
    }

 void sendPositionCellValNotSetCoords(Coordinate pos, uint8_t cellVal){
  String toSend = "";
  int xSend, ySend;
  

  xSend = pos.getX();
  ySend = pos.getY();
  
  toSend += updateCommand + dl + xSend + dl + ySend + dl;
  Serial.print(toSend);
  Serial.println(cellVal, DEC);
  
  }       
