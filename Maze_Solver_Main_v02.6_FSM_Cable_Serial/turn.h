#define _ROTATE_TIME 900
#include <Servo.h>

#define rightWheel 12
#define leftWheel 13

Servo servoLeftWheel;
Servo servoRightWheel;

void initServo();

void moveForward();

void moveLeft();

void moveRight();

void stop() ;
