
void initializeSerial();

String readSerialLine();

void populateMazeArray();

void debugPrint(String stringIn);
void debugPrint(char * stringIn);
void echoPrint(String stringIn);
void echoPrint(char * stringIn);

void updatePosition(int xIn, int yIn, uint8_t cellVal);

//Behaviour depends on testing mode - either will actually try and learn about pos, or will just transmit markings from current maze spot
void sendCurrentPosition();

void sendPosition(Coordinate pos);

void sendPositionNotSetCoords(Coordinate pos);
void sendPositionCellValNotSetCoords(Coordinate pos, uint8_t cellVal);
