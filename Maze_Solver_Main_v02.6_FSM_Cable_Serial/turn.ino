#include <Servo.h>

void initServo(){
  servoLeftWheel.attach(leftWheel); 
  servoRightWheel.attach(rightWheel);
  stop();
  }


void moveForward() {
  servoLeftWheel.writeMicroseconds(1700);
  servoRightWheel.writeMicroseconds(1300);
}

void moveLeft() {
  servoLeftWheel.writeMicroseconds(1300);
  servoRightWheel.writeMicroseconds(1300);
  delay(_ROTATE_TIME);
  servoLeftWheel.writeMicroseconds(1700);
  servoRightWheel.writeMicroseconds(1300);
}

void moveRight() {
  servoLeftWheel.writeMicroseconds(1700);
  servoRightWheel.writeMicroseconds(1700);
  delay(_ROTATE_TIME);
  servoLeftWheel.writeMicroseconds(1700);
  servoRightWheel.writeMicroseconds(1300);
}

void stop() {
  servoLeftWheel.writeMicroseconds(1500);
  servoRightWheel.writeMicroseconds(1475); // or 1500, 1488.5
  //delay(200);
}

