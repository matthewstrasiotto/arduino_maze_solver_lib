#include "Bitfields.h"
#include "Maze.h"
#include "Coordinate.h"
#include "boardIOKeywords.h"
#include "boardIO.h"
#include "Command.h"
#include "FSM_enums.h"
#include "fsms.h"
#include "turn.h"

using namespace main_fsm;
namespace r = rotate_fsm;
namespace mv = move_fsm;

//using namespace rotate_fsm;
//using namespace move_fsm;



mainState mS = recieving;
mv::moveState mvS = mv::forward;
r::rotateState rS = r::start;
String mainLine = "";

boolean moveDebug = true;
boolean ioDebug = false;
boolean testingMode = false;
boolean echoing = false;

#define _MAX_TOKEN_PER_LINE 6
#define _BAUDRATE 9600

Maze myMaze;
Coordinate prev, current;
using namespace D;
Direction prevDir, currentDir, dirToTurn;

bool relativeLeft, relativeRight, forward, backward;
bool doneRotating;
bool doneMoving;

namespace biok = boardIOKeywords;
void setup() {
  initServo();
  initializeSerial();
}

void loop() {

   switch (mS){
    case recieving:
      mainLine = "";
      if (Serial.available() > 0){
        mainLine = readSerialLine();
        }
      mS = deciding;  
    break;
    case deciding:

    if (mainLine.equals(biok::moveOnce)){
      prev = myMaze.getCurrentPos();
      prevDir = prev.getDir();
      myMaze.move();
      current = myMaze.getCurrentPos();
      currentDir = current.getDir();


      mS = toMove;
      
      return;
      }
      
      mS = recieving;
    break;
    case toMove:
     
        mS = moving;
    case moving:
     if (fsm_move() == true) mS = transmitting;      
    break;
    case transmitting:
      sendPositionNotSetCoords(prev);
      sendPosition(current);

      mS = recieving;
    break;
    }
  
    
}



