bool fsm_move(){

   switch (mvS){
    case mv::start:
        doneRotating = false;        
    case mv::deciding:
        //Do I need to rotate?
        if (doneRotating == false){ 
          mvS = mv::rotating;
        } else {
          mvS = mv::forward;
        }
  
      
    break;
    case mv::rotating:
      if (moveDebug == true) debugPrint("Rotating!");
      doneRotating = fsm_rotate();

      mvS = mv::deciding;
    break;
    case mv::forward:
      if (moveDebug == true) debugPrint("Forward mode!");
      moveForward();
      delay(_ROTATE_TIME);
      stop();
      mvS = mv::done;
    break;
    case mv::done:
      mvS = mv::start;
      return true;
    //break;
    
    }
  return false;
  }

bool fsm_rotate(){
  switch (rS){
    case r::start:


      rS = r::deciding;
    case r::deciding:
      if (currentDir == Coordinate::turnLeftNotRight(prevDir, true)) rS = r::left;
      if (currentDir == Coordinate::turnLeftNotRight(prevDir, false)) rS = r::right;
      if (currentDir == Coordinate::turnAround(prevDir)) rS = r::left;

      if (currentDir == prevDir) rS = r::done;      
    break;
    case r::left:
      moveLeft();
      prevDir = Coordinate::turnLeftNotRight(prevDir, true);
      rS = r::deciding;
    break;
    case r::right:
      moveRight();
      prevDir = Coordinate::turnLeftNotRight(prevDir, false);
      rS = r::deciding;
    break;
    case r::done:
    
      rS = r::start;
    return true;
    
    }


  return false;
  }


