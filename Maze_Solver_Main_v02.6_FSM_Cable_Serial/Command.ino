namespace biok = boardIOKeywords;
void processLine(String lineIn){
  
  String tokens[_MAX_NUMBER_TOKENS];
  
  int x = 0, y = 0;
  uint8_t cellVal = 0x00;
  int i = 0, wordIndex = 0, totalWords = 1;
  char c = 0;

  for (i = 0; i < _MAX_NUMBER_TOKENS; i ++){
    tokens[i] = "";
    }

  if (commandDebug == true) Serial.print(biok::comment + biok::dl + biok::echo + biok::dl);
  for (i = 0; i < lineIn.length() && wordIndex < _MAX_NUMBER_TOKENS; i++){
    c = lineIn.charAt(i);
    
    switch (c){
      case ':':
        //If its the delimeter, dont add it to this word, and increment wordIndex so the next char
        //Is added to the next word
        wordIndex++;
        if (commandDebug == true) Serial.print(' ');
      break;
      case '\n':
      
      break;
      default:
        if (commandDebug == true) Serial.print(c);
        tokens[wordIndex].concat(c);
      break;      
      }
    }
  Serial.println();  
  totalWords = wordIndex;
  if (totalWords < _MAX_NUMBER_TOKENS) totalWords++;
  
  if (tokens[0].equals(biok::updateCommand)){
    if (totalWords < 4) return;
      x = tokens[1].toInt();
      y = tokens[2].toInt();
      cellVal = (uint8_t) tokens[3].toInt();

      updatePosition(x, y, cellVal);
    }
  
  }
