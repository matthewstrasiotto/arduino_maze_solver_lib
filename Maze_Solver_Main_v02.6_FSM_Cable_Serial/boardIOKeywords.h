//Delimeter is :, and \n

namespace boardIOKeywords{
  
static String dl = ":";

  //static String inputKeyword = "POUT";
  //static String outputKeyword = "BOARDOUT";
  
  static String testCommand = "TST";
  static String updateCommand = "U";
  
  static String positionUpdate = "POS";
  
  static String xCommand = "XVAL", yCommand = "YVAL";
  
  static String cellValCommand = "CELLVAL";
  
  static String yes = "TR", no = "FLS";
  static String acknowledge = "TY";
  static String nAcknowledge = "ST";
  static String moveOnce = "MV";
  
  static String comment = "IG";
  static String echo = "EC";
  static String sendMap = "SM";
  }
