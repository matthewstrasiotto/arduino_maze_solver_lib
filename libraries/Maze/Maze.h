

#ifndef __MAZE_H__
#define __MAZE_H__

#include "Bitfields.h"
#include "Arduino.h"

//#include <cstdlib>
#include "Coordinate.h"
//#include <array>
//#include <cstdint>
//#include <array>



/*
CELL INFO
All 0s on leading 4 bits (leftmost) = 4 way junction
3 0s on leading 4 = 3 way junction
2 0s on leading 4 = path
1 0 = dead end

next 2 bits indicate path marking,
if none filled, havent entered/exited that path from junction
if one is filled, have entered/exited that spot once
if downh bits are filled, have entered twice. 
This encoding allows for an additional state (2 bits encodes 4 states) could use to track 
whether entered or exited, but no need

Additionally, 2 unused bits, I dont know know what I'll use these for.

1xxx - Left 
x1xx - Up
xx1x - Right
xxx1 - Down

xxxx 11xx - Marks

TRAVERSAL INFO - if at a junction (ie, one or less adjacent wall),
add a mark to the path thru which u came, and once you choose your next path,
mark that also

Never traverse doubly marked path, and if choosing between paths, 
choose that with least marks (choose 0 over 1)

To define some more behaviour:
	Each time you're going through a cell, attempt to move in your current direction
		before you move, check if that's a valid move
			ie. is there no wall there?
			
			am i at a junction?
				if i'm at a junction - how many marks does the spot I want to move to have?
					if it's 2, the spot is invalid
					if it's 1, i need to check the positions to my left and right, and determine if they're better (have less marks)
					choose the one with the least marks (0, ideally), and if theyre equal, probably go left as a preference (arbitrary)
					note that if all 3 directions are invalid, i should 180


In the LSB, a 1 indicates that this position has been discovered
*/





#define _GRID_SIZE 16
//using std::array;

using namespace D;

class Maze{
	public:
		
		static uint8_t generateCell(bool hasLeft, bool hasRight, bool hasFront, Direction bearing);
		
		Maze();
		//Maze(std::array<std::array<uint8_t, _GRID_SIZE>, _GRID_SIZE> inputHidden);
		
		Direction nextDir();
		void move();		
		
		//Following's behaviour is dependant on whether it we are in testing mode
		void updateCurrentPos(uint8_t cellValIn);
		void updatePosAt(Coordinate pos, uint8_t cellValIn);
		
		uint8_t countPaths(Coordinate pos);
		bool isJunction(Coordinate pos);
		
		// uint8_t countMarks(Coordinate pos);
		
		// uint8_t countMarksDirection(Coordinate pos, Direction travelDir);
		
		// uint8_t countCurrentMarks();
		
		Coordinate findNext(Coordinate pos);
		
		bool currentIsJunction();
		bool currentDeadEnd();
		//bool currentSeen();
		
		
		bool hasDirection(Coordinate pos, Direction dir);
		
		bool currentHasDir(Direction dirIn);
		bool currentHasTravelDir();
		
		bool validMoveFromCurrent(Direction travelDir);
		

		bool currentHasLeft();                           // Solve Class
		bool currentHasUp();                             // Solve Class
		bool currentHasRight();                          // Solve Class
		bool currentHasDown();                           // Solve Class
                                                         
		bool hasLeft(Coordinate pos);                    // Maze Class
		bool hasUp(Coordinate pos);                      // Maze Class
		bool hasRight(Coordinate pos);                   // Maze Class
		bool hasDown(Coordinate pos);                    // Maze Class
	
		//bool isSeen(Coordinate pos);
		bool isDeadEnd(Coordinate pos);
		
		uint8_t cellVals(Coordinate pos);
		
		bool isValidMove(Coordinate pos, Direction travelDir);
		
		Coordinate getCurrentPos();
		
		bool posInBounds(Coordinate pos);
		
		
	private:			
		void _resize(Direction dir);
	
		//bool _testingMode;
		
		// // void _mark(Coordinate pos);			//All of these will go
		// // void _markCurrent();                //All of these will go
		                                    // All of these will go
		// // void _markSeen(Coordinate pos);     //All of these will go
		// // void _markCurrentSeen();            //All of these will go
		
		
		
		void _markEdgeDir(Coordinate pos, Direction edgeDir);
		
		void _markLeft(Coordinate pos);
		void _markUp(Coordinate pos);
		void _markRight(Coordinate pos);
		void _markDown(Coordinate pos);
		
		void _markWithBit(Coordinate pos, uint8_t bitfield);
		
		bool _bfMarkAtPos(Coordinate pos, uint8_t bitfield);
		
		bool _isDirMarked(Coordinate pos, Direction dir);
		
		bool _isUpMarked(Coordinate pos);
		bool _isLeftMarked(Coordinate pos);
		bool _isRightMarked(Coordinate pos);
		bool _isDownMarked(Coordinate pos);
		
		
		Coordinate _prevPos1, _prevPos2, _currentPos;
		
		
		bool _junctions[_GRID_SIZE][_GRID_SIZE];
		uint8_t _known[_GRID_SIZE][_GRID_SIZE];
		
		
		void _moveUpdatePrevious(Direction travelDir);
		
		Coordinate _getPos(Coordinate posIn);
};

#endif //__MAZE_H__
