#include "Bitfields.h"
#include "Maze.h"
#include "Coordinate.h"


/*
FOLLOWING IS NO LONGER TRUE

The following will rely on the #define TESTING to determine where
the board will get its input array - basically if testing, the array
 will be assigned in a preprocessor conditional, and when updating, will 
 use that conditional to decide whether it takes its input from the function 
 being called by board, or 
*/


//using std::array;
using namespace D;


//Constructors:
	
Maze::Maze(){
	int i = 0, j = 0;
	
	
	//Fill the 2d array with 0 values
	for (i = 0; i < _GRID_SIZE ; i++){
		
		for (j = 0; j < _GRID_SIZE ; j++){
			
			this->_known[j][i] = 0x00;
		}
	}
	
	
	//Here, if in testing mode, populate it with a known maze
	//TODO: **** Actually - here I think I'll set up a method that allows processing to send the board all of the current map data
	
	int lastRow = _GRID_SIZE - 1;
	int lastCol = _GRID_SIZE -1;
	int startCol = lastCol/2;
	
	_currentPos =  Coordinate(startCol, lastRow);
	_prevPos1 = Coordinate(_currentPos);
	_prevPos2 = Coordinate(_currentPos);
	
}

//Cell generation
static uint8_t generateCell(bool leftWall, bool rightWall, bool fwdWall, Direction bearing){
	uint8_t out = 0x00;
	
	switch (bearing){
		case up:
		if (leftWall == true){
		out |= Bitfields::leftBit;
		}
		if (fwdWall == true){
			out |= Bitfields::upBit;
		}
		if (rightWall == true){
			out |= Bitfields::rightBit;
		}
		
		break;
		
		case left:
		if (leftWall == true){
		out |= Bitfields::downBit;
		}
		if (fwdWall == true){
			out |= Bitfields::leftBit;
		}
		if (rightWall == true){
			out |= Bitfields::upBit;
		}
		
		break;
		
		case down:
		if (leftWall == true){
		out |= Bitfields::rightBit;
		}
		if (fwdWall == true){
			out |= Bitfields::downBit;
		}
		if (rightWall == true){
			out |= Bitfields::leftBit;
		}
		break;
		case right:
		if (leftWall == true){
		out |= Bitfields::upBit;
		}
		if (fwdWall == true){
			out |= Bitfields::rightBit;
		}
		if (rightWall == true){
			out |= Bitfields::downBit;
		}
		break;
	}
	
	return out;
}


void Maze::updateCurrentPos(uint8_t currentCell){
	this->updatePosAt(this->_currentPos, currentCell);
	//this->_markCurrentSeen();
}

void Maze::updatePosAt(Coordinate pos, uint8_t inputCell){
	int x = pos.getX();
	int y = pos.getY();
	
	this->_known[y][x] |= inputCell;
	
}

////////////////////////////////////////////
//			Positional Cell Determinations
////////////////////////////////////////////

//Return cell details at a position
uint8_t Maze::cellVals(Coordinate pos){
	int x = pos.getX();
	int y = pos.getY();
	uint8_t out = this->_known[y][x];
	return out;
}


//////////////////////////////
//    Bitfield bools     //
//////////////////////////////

uint8_t Maze::countPaths(Coordinate pos){
	uint8_t out = 0;
	if (this->hasLeft(pos)){
		out++;
	}
	if (this->hasUp(pos)){
		out++;
	}
	if (this->hasRight(pos)){
		out++;
	}
	if (this->hasDown(pos)){
		out++;
	}
	return out;
}

bool Maze::isJunction(Coordinate pos){
	int x = pos.getX();
	int y = pos.getY();
	
	if (this->countPaths(pos) > 2){
		_junctions[y][x] = true;
		return true;
	}
	return _junctions[y][x];
}

bool Maze::isDeadEnd(Coordinate pos){
	if (this->countPaths(pos) < 2){
		return true;
	}
	return false;
}

// uint8_t Maze::countMarks(Coordinate pos){
	// uint8_t cell = this->cellVals(pos);
	// uint8_t m1 = ((Bitfields::m1Bit & cell) >> Bitfields::m1Shift);
	// uint8_t m2 = ((Bitfields::m2Bit & cell) >> Bitfields::m2Shift);
	
	// uint8_t marks = m1 + m2;
	// return marks;
// }


bool Maze::hasLeft(Coordinate pos){
	uint8_t cellMask = cellVals(pos);
	cellMask &= Bitfields::leftBit;
	if (cellMask != 0){
		 return false;
	 }
	return true;
}

bool Maze::hasUp(Coordinate pos){
	uint8_t cellMask = cellVals(pos);
	cellMask &= Bitfields::upBit;
	if (cellMask != 0){
		 return false;
	 }
	return true;
}

bool Maze::hasRight(Coordinate pos){
	uint8_t cellMask = cellVals(pos);
	cellMask &= Bitfields::rightBit;
	if (cellMask != 0){
		 return false;
	 }
	return true;
}

bool Maze::hasDown(Coordinate pos){
	uint8_t cellMask = cellVals(pos);
	cellMask &= Bitfields::downBit;
	if (cellMask != 0){
		 return false;
	 }
	return true;
}

// bool Maze::isSeen(Coordinate pos){
	 // if (Bitfields::seenBit & this->cellVals(pos) != 0){
		 // return false;
	 // }
	// return true;
// }

bool Maze::hasDirection(Coordinate pos, Direction dir){
	
	switch (dir){
		case up:
		return this->hasUp(pos);
		break;
		case left:
		return this->hasLeft(pos);
		break;
		case right:
		return this->hasRight(pos);
		break;
		case down:
		return this->hasDown(pos);
		break;
	}
	return false;
}



//////////////////////////////////////
//				bitfield marking 
///////////////////////////////////////
// void Maze::_mark(Coordinate pos){
	// uint8_t numMarks = this->countMarks(pos);
	// int x = pos.getX();
	// int y = pos.getY();
	// if (numMarks == 0x00){
		// this->_known[y][x] |= Bitfields::m1Bit;
	// }
	// if (numMarks == 0x01){
		// this->_known[y][x] |= Bitfields::m1Bit | Bitfields::m2Bit;
	// }
// }


// void Maze::_markSeen(Coordinate pos){
	// int x = pos.getX();
	// int y = pos.getY();
	// this->_known[y][x] |= Bitfields::seenBit;
// }

// void Maze::_markCurrentSeen(){
	// this->_markSeen(this->_currentPos);
// }

// void Maze::_markCurrent(){
	// this->_mark(this->_currentPos);
// }

/////////////////////////////////////////
//				Current pos bools
/////////////////////////////////////////

// bool Maze::currentSeen(){
	// return this->isSeen(this->_currentPos);
// }

bool Maze::currentHasLeft(){
	return this->hasLeft(this->_currentPos);
}

bool Maze::currentHasUp(){
	return this->hasUp(this->_currentPos);
}

bool Maze::currentHasRight(){
	return this->hasRight(this->_currentPos);
}

bool Maze::currentHasDown(){
	return this->hasDown(this->_currentPos);
}

bool Maze::currentIsJunction(){
	return this->isJunction(this->_currentPos);
}

bool Maze::currentHasDir(Direction dirIn){
	return this->hasDirection(this->_currentPos, dirIn);
}

bool Maze::currentHasTravelDir(){
	return this->currentHasDir(this->_currentPos.getDir());
}

bool Maze::validMoveFromCurrent(Direction travelDir){
	return (this->isValidMove(this->_currentPos, travelDir));
}

bool Maze::currentDeadEnd(){
	return this->isDeadEnd(this->_currentPos);
}
//////////////////////////////////////
//		Movement functions
//////////////////////////////////////

//The following assumes the current cell has already been updated
void Maze::move(){
	//This assumes we've updated current already
	
	Direction currentDir = this->_currentPos.getDir();
	Direction relativeLeft = Coordinate::turnLeftNotRight(currentDir, true);
	Direction relativeRight = Coordinate::turnLeftNotRight(currentDir, false);
	Direction backwards = Coordinate::turnAround(currentDir);
	
	bool frontOpen = this->currentHasTravelDir();
	bool leftOpen = this->currentHasDir(relativeLeft), rightOpen = this->currentHasDir(relativeRight);
	bool backOpen = this->currentHasDir(backwards);
	
	if (isJunction(_currentPos) == false){
		if (frontOpen == true){
			if (this->validMoveFromCurrent(currentDir)){
				this->_moveUpdatePrevious(currentDir);
				return;
				//return
			}
			
		}
		if (leftOpen == true){
			if (this->validMoveFromCurrent(relativeLeft)){
				this->_moveUpdatePrevious(relativeLeft);
				return; 
				//return
			}
		}
		if (rightOpen == true){
			if (this->validMoveFromCurrent(relativeRight)){
				this->_moveUpdatePrevious(relativeRight);
				return; 
				//return
			}
		}
		
		//Otherwise turn around and go backwards
		this->_moveUpdatePrevious(backwards);
		return;
		//return
	}
	//Current is junction, count the marks of each direction, incl. back
	//Ensure that previous is updated with a mark
	
	// uint8_t leastMarks = 0x02;
	// uint8_t backMarks = countMarksDirection(_currentPos, backwards);
	// uint8_t rightMarks = countMarksDirection(_currentPos, relativeRight);
	// uint8_t leftMarks = countMarksDirection(_currentPos, relativeLeft);
	// uint8_t frontMarks = countMarksDirection(_currentPos, currentDir);
	
	
//	leastMarks = backMarks;
	Direction validDirWithLeastMarks = currentDir;
	bool unmarkedYet = true;
	bool leftBest = false, rightBest = false, backBest = false, frontBest = false;
	//So if a cell is unmarked, traverse it, preferably go forward first
	
	if (frontOpen == true){
		validDirWithLeastMarks = currentDir;
		frontBest = (_isDirMarked(_currentPos, currentDir) == false);
		
	}
	if (leftOpen == true){
		validDirWithLeastMarks = relativeLeft;
		leftBest = (_isDirMarked(_currentPos, relativeLeft) == false);
	}
	if (rightOpen == true){
		validDirWithLeastMarks = relativeRight;
		rightBest = (_isDirMarked(_currentPos, relativeRight) == false);
	}
	if (backOpen == true){
		validDirWithLeastMarks = backwards;
		backBest = (_isDirMarked(_currentPos, backwards) == false);
	}
	
	if (backBest) validDirWithLeastMarks = backwards;
	if (rightBest) validDirWithLeastMarks = relativeRight;
	if (leftBest) validDirWithLeastMarks = relativeLeft;
	if (frontBest) validDirWithLeastMarks = currentDir;


	
	this->_moveUpdatePrevious(validDirWithLeastMarks);
	
	
}

bool Maze::posInBounds(Coordinate pos){
	int x = pos.getX();
	int y = pos.getY();
	
	if (x < 0 || x >= _GRID_SIZE){
		return false;
	}
	
	if (y < 0 || y >= _GRID_SIZE){
		return false;
	}
	
	return true;
}

bool Maze::isValidMove(Coordinate pos, Direction travelDir){
		
	if (this->hasDirection(pos, travelDir) == false){
		return false;
	}
	//Coordinate::copyAndMove(pos, next, travelDir);
	Coordinate next(pos);
	next.move(travelDir);
	
	if (this->posInBounds(next) == false){
		return false;
	}
	
	// if (this->countMarks(next) > 0x01){
		// return false;
	// }
	
	return true;
}


void Maze::_moveUpdatePrevious(Direction travelDir){
	Coordinate prev(_currentPos);
	
	_currentPos.move(travelDir);
	Direction lookBack = Coordinate::turnAround(travelDir);
	
	if (isJunction(prev) || isJunction(_currentPos)){
		_markEdgeDir(_currentPos, lookBack);
		_markEdgeDir(prev, travelDir);
	}
	
}

Coordinate Maze::_getPos(Coordinate posIn){
	return posIn;
}

Coordinate Maze::getCurrentPos(){
	return this->_getPos(_currentPos);
}

// uint8_t Maze::countMarksDirection(Coordinate pos, Direction travelDir){
	// // if (this->isValidMove(pos, travelDir) == false){
		// // return 0x02;
	// // }
	
	// // if (this->hasDirection(pos, travelDir) == false){
		// // return 0x02;
	// // }
	
	// Coordinate next(pos);
	// next.move(travelDir);
	// //Coordinate::copyAndMove(pos, next, travelDir);
	// uint8_t out = this->countMarks(next);
	
	// return out;
// }

void Maze::_markEdgeDir(Coordinate pos, Direction edgeDir){
	
	switch (edgeDir){
		case left:
			_markLeft(pos);
		break;
		case up:
			_markUp(pos);
		break;
		case right:
			_markRight(pos);
		break;
		case down:
			_markDown(pos);
		break;
		
	}
	
	
}

void Maze::_markLeft(Coordinate pos){
	if (_bfMarkAtPos(pos, Bitfields::leftMBit)){
		_markWithBit(pos, Bitfields::leftBit);
	}
	_markWithBit(pos, Bitfields::leftMBit);
}

void Maze::_markUp(Coordinate pos){
	if (_bfMarkAtPos(pos, Bitfields::upMBit)){
		_markWithBit(pos, Bitfields::upBit);
	}
	_markWithBit(pos, Bitfields::upMBit);
}

void Maze::_markRight(Coordinate pos){
	if (_bfMarkAtPos(pos, Bitfields::rightMBit)){
		_markWithBit(pos, Bitfields::rightBit);
	}
	_markWithBit(pos, Bitfields::rightMBit);
}

void Maze::_markDown(Coordinate pos){
	if (_bfMarkAtPos(pos, Bitfields::downMBit)){
		_markWithBit(pos, Bitfields::downBit);
	}
	_markWithBit(pos, Bitfields::downMBit);
}

void Maze::_markWithBit(Coordinate pos, uint8_t bitfield){
	int x = pos.getX();
	int y = pos.getY();
	_known[y][x] |= bitfield;
}

bool Maze::_bfMarkAtPos(Coordinate pos, uint8_t bitfield){
	uint8_t cell = this->cellVals(pos);
	
	cell &= bitfield;
	
	if (cell != 0x00) return true;
	return false;
}

bool Maze::_isUpMarked(Coordinate pos){
	return _bfMarkAtPos(pos, Bitfields::upMBit);
}
bool Maze::_isLeftMarked(Coordinate pos){
	return _bfMarkAtPos(pos, Bitfields::leftMBit);
}
bool Maze::_isRightMarked(Coordinate pos){
	return _bfMarkAtPos(pos, Bitfields::rightMBit);
}
bool Maze::_isDownMarked(Coordinate pos){
	return _bfMarkAtPos(pos, Bitfields::downMBit);
}

bool Maze::_isDirMarked(Coordinate pos, Direction dir){
	switch (dir){
		case up:
		return _isUpMarked(pos);
		case left:
		return _isLeftMarked(pos);
		case right:
		return _isRightMarked(pos);
		case down:
		return _isDownMarked(pos);		
	}
	return true;
}

