#ifndef _BIT_FIELDS_H
#define _BIT_FIELDS_H

#include "Arduino.h"

#define _bs(n) (0x01 << n)

class Bitfields{
public:
	static const uint8_t leftShift = 7;
	static const uint8_t upShift = 6;
	static const uint8_t rightShift = 5;
	static const uint8_t downShift = 4;

	static const uint8_t m2Shift = 3;
	static const uint8_t m1Shift = 2;
	static const uint8_t unusedShift = 1;
	static const uint8_t seenShift = 0;

	static const uint8_t leftBit = (_bs(leftShift));
	static const uint8_t upBit =  (_bs(upShift));
	static const uint8_t rightBit = (_bs(rightShift));
	static const uint8_t downBit = (_bs(downShift));
	static const uint8_t leftMBit = (_bs(m2Shift));
	static const uint8_t upMBit = (_bs(m1Shift));
	static const uint8_t rightMBit = (_bs(unusedShift));
	static const uint8_t downMBit =  (0x01);

};

#undef _bs

#endif
