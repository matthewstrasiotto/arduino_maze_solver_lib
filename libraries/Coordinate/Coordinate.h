#ifndef __COORD_H__
#define __COORD_H__

#include "Arduino.h"

//#include "Direction.h"
namespace D{
	enum Direction {up = 0, left = 1, down = 2, right = 3};
}

using namespace D;
class Coordinate {
	public:
		Coordinate();
		Coordinate(int xIn, int yIn);
		Coordinate(int xIn, int yIn, Direction dirIn);
		
		//Coordinate(const Coordinate& copyOf);
		
		//Constructor for returning new coordinate based on input coord moving in direction
		//Coordinate(Coordinate startingPos, Direction movementDir);
		
		void move(Direction dir);
		void moveCurrentDir();
		// static void copyCoordinate(Coordinate src, Coordinate dest);						  //Remove me probably
		// static void copyAndMove(Coordinate src, Coordinate dest, Direction movementDir);      //Remove me probably
		
		
		
		void setCoords(int xIn, int yIn);
		void setX(int xIn);
		void setY(int yIn);
		void setDir(Direction dirIn);
		
		static Direction turnLeftNotRight(Direction dirIn, bool leftTurn);
		static Direction turnAround(Direction dirIn);
		
		int getX();
		int getY();
		
		Direction getDir();
	private:		
		
		int _x, _y;
		Direction _dir;
};

#endif // __COORD_H__
