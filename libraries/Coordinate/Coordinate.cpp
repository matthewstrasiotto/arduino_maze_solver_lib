#include "Coordinate.h"
//#include "Direction.h"


Coordinate::Coordinate(){
	this->_x = 0;
	this->_y = 0;
	this->_dir = up;
}

Coordinate::Coordinate(int xIn, int yIn){
	this->_x = xIn;
	this->_y = yIn;
	this->_dir = up;
}

Coordinate::Coordinate(int xIn, int yIn, Direction dirIn){
	this->_x = xIn;
	this->_y = yIn;
	setDir(dirIn);
}

// Coordinate::Coordinate(const Coordinate& copyOf){
	// this->_x = copyOf.getX();
	// this->_y = copyOf.getY();
	// this->_dir = copyOf.getDir();
// }

// Coordinate::Coordinate(Coordinate startingPos, Direction movementDir){
	// this->_x = startingPos.getX();
	// this->_y = startingPos.getY();
	// setDir(movementDir);
	
	// moveCurrentDir();
// }


void Coordinate::move(Direction dir){
	
	
	switch (dir){
	case up:
			this->setY(this->getY() - 1);
			
			break;
	case left:
			this->setX(this->getX() - 1);
			
			break;

	case down:
			this->setY(this->getY() + 1);
			break;
	case right:
			this->setX(this->getX() + 1);
			break;
	}
	
	this->setDir(dir);
}

void Coordinate::moveCurrentDir(){
	move(this->getDir());
}

void Coordinate::setX(int xIn){
	this->_x = xIn;
}
void Coordinate::setY(int yIn){
	this->_y = yIn;
}
void Coordinate::setDir(Direction dirIn){
	this->_dir = dirIn;
}

int Coordinate::getX(){
	return this->_x;
}
int Coordinate::getY(){
	return this->_y;
}
Direction Coordinate::getDir(){
	return this->_dir;
}

Direction Coordinate::turnLeftNotRight(Direction dirIn, bool leftTurn){
	Direction out = up;
	if (leftTurn == true){
		
		switch (dirIn){
			case up:
			out = left;
			break;
			case left:
			out = down;
			break;
			case down:
			out = right;
			break;
			case right:
			out = up;
			break;
		}
		
	return out;	
	}
	
	switch (dirIn){
		case up:
		out = right;
		break;
		case right:
		out = down;
		break;
		case down:
		out = left;
		break;
		case left:
		out = up;
		break;
	}
	return out;
}

Direction Coordinate::turnAround(Direction dirIn){
	Direction d2, out;
	d2 = turnLeftNotRight(dirIn, true);
	out = turnLeftNotRight(d2, true);
	return out;
}

// void Coordinate::copyCoordinate(Coordinate src, Coordinate dest){
	// dest.setX(src.getX());
	// dest.setY(src.getY());
	// dest.setDir(src.getDir());
// }

// void Coordinate::copyAndMove(Coordinate src, Coordinate dest, Direction movementDir){
	// Coordinate::copyCoordinate(src, dest);
	// dest.move(movementDir);
// }